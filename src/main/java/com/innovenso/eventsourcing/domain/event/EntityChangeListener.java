package com.innovenso.eventsourcing.domain.event;

import com.innovenso.eventsourcing.api.event.EntityEvent;
import com.innovenso.eventsourcing.api.exception.EntityChangeVetoException;

public interface EntityChangeListener<T extends EntityEvent> {
  void onChange(T event) throws EntityChangeVetoException;
}
