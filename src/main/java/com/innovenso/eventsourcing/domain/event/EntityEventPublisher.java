package com.innovenso.eventsourcing.domain.event;

import com.innovenso.eventsourcing.api.event.EntityEvent;
import com.innovenso.eventsourcing.api.event.EntityEventListener;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import lombok.NonNull;

public class EntityEventPublisher<T extends EntityEvent> {
  private final Set<EntityEventListener<T>> eventListeners;

  public EntityEventPublisher() {
    this(Set.of());
  }

  public EntityEventPublisher(@NonNull final Collection<EntityEventListener<T>> listeners) {
    this.eventListeners = new HashSet<>();
    this.eventListeners.addAll(listeners);
  }

  @SafeVarargs
  public final void setEventListeners(EntityEventListener<T>... listeners) {
    this.eventListeners.clear();
    this.eventListeners.addAll(Set.of(listeners));
  }

  public final void setEventListeners(Collection<EntityEventListener<T>> listeners) {
    this.eventListeners.clear();
    this.eventListeners.addAll(listeners);
  }

  public final void publish(@NonNull final T event) {
    eventListeners.forEach(listener -> listener.onEvent(event));
  }
}
