package com.innovenso.eventsourcing.domain.entity;

import com.innovenso.eventsourcing.api.event.EntityEvent;
import com.innovenso.eventsourcing.api.id.EntityId;
import com.innovenso.eventsourcing.api.id.EntityRevisionId;
import com.innovenso.eventsourcing.api.id.RevisionId;
import java.util.*;
import java.util.stream.Collectors;
import lombok.NonNull;

public class EntityHistory<T extends EntityEvent> {
  private Map<RevisionId, T> events;
  private final EntityId entityId;

  public EntityHistory(@NonNull final EntityId entityId) {
    this.events = new HashMap<>();
    this.entityId = entityId;
  }

  public void addEvent(@NonNull final T event) {
    if (!entityId.equals(event.getId().getEntityId()))
      throw new IllegalArgumentException("event does not belong to this entity");
    events.put(event.getId().getNewRevisionId(), event);
  }

  public T getParent(@NonNull final T event) {
    return getEvent(event.getId().getParentRevisionId());
  }

  public T getEvent(@NonNull final RevisionId revisionId) {
    return events.get(revisionId);
  }

  public List<T> getAllEvents() {
    return events.values().stream()
        .sorted(new EntityEventComparator())
        .collect(Collectors.toList());
  }

  public T getFirst(@NonNull final RevisionId revisionId) {
    T first = null;
    EntityEventIterator iterator = new EntityEventIterator(revisionId);
    while (iterator.hasNext()) {
      first = iterator.next();
    }
    return first;
  }

  public RevisionId getLatestRevisionId() {
    T event = getAllEvents().stream().reduce((first, second) -> second).orElse(null);
    if (event == null) return EntityRevisionId.getInitialInstance(this.entityId).getRevisionId();
    else return event.getId().getNewRevisionId();
  }

  public Iterator<T> iterator(@NonNull final RevisionId revisionId) {
    return new EntityEventIterator(revisionId);
  }

  public List<T> getSnapshot(@NonNull final RevisionId revisionId) {
    List<T> accountEventList = new ArrayList<>();
    Iterator<T> iterator = iterator(revisionId);
    while (iterator.hasNext()) {
      accountEventList.add(iterator.next());
    }
    Collections.reverse(accountEventList);
    return accountEventList;
  }

  public class EntityEventIterator implements Iterator<T> {
    private T next;

    public EntityEventIterator(@NonNull final RevisionId revisionId) {
      next = getEvent(revisionId);
    }

    @Override
    public boolean hasNext() {
      return next != null;
    }

    @Override
    public T next() {
      if (!hasNext()) return null;
      else {
        T current = next;
        if (current.getId().hasParent()) {
          next = getEvent(current.getId().getParentRevisionId());
        } else {
          next = null;
        }
        return current;
      }
    }
  }

  public class EntityEventComparator implements Comparator<T> {

    @Override
    public int compare(T event1, T event2) {
      int timestampCompare =
          Long.compare(
              event1.getId().getNewRevisionId().getTimestamp(),
              event2.getId().getNewRevisionId().getTimestamp());
      if (timestampCompare != 0) return timestampCompare;
      else
        return Long.compare(
            event1.getId().getParentRevisionId().getTimestamp(),
            event2.getId().getParentRevisionId().getTimestamp());
    }
  }
}
