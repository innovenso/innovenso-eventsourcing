package com.innovenso.eventsourcing.domain.entity;

import com.innovenso.eventsourcing.api.command.EntityCommand;
import com.innovenso.eventsourcing.api.entity.Entity;
import com.innovenso.eventsourcing.api.entity.EntityInteractionHandler;
import com.innovenso.eventsourcing.api.entity.EntityMutableState;
import com.innovenso.eventsourcing.api.entity.EntityReadModel;
import com.innovenso.eventsourcing.api.event.EntityEvent;
import com.innovenso.eventsourcing.api.exception.EntityCommandFailedException;
import com.innovenso.eventsourcing.api.exception.InvalidCommandException;
import com.innovenso.eventsourcing.api.id.EntityId;
import com.innovenso.eventsourcing.api.id.EntityRevisionId;
import com.innovenso.eventsourcing.api.id.EventId;
import com.innovenso.eventsourcing.api.id.RevisionId;
import com.innovenso.eventsourcing.domain.event.EntityChangeListener;
import java.util.Set;
import java.util.function.BiFunction;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import lombok.NonNull;
import lombok.extern.java.Log;

@Log
public abstract class AbstractEntity<
        CommandType extends EntityCommand,
        EventType extends EntityEvent,
        ReadModelType extends EntityReadModel,
        MutableStateType extends EntityMutableState>
    implements Entity<ReadModelType, MutableStateType> {
  private EntityRevisionId id;
  private EntityChangeListener<EventType> changeListener;
  private Validator validator;
  private final Set<
          ? extends
              EntityInteractionHandler<
                  ? extends CommandType, ? extends EventType, ReadModelType, MutableStateType>>
      interactionHandlers;

  @SafeVarargs
  public AbstractEntity(
      EntityInteractionHandler<
              ? extends CommandType, ? extends EventType, ReadModelType, MutableStateType>...
          interactionHandlers) {
    this(new EntityId(), interactionHandlers);
  }

  @SafeVarargs
  public AbstractEntity(
      @NonNull final EntityId entityId,
      EntityInteractionHandler<
              ? extends CommandType, ? extends EventType, ReadModelType, MutableStateType>...
          interactionHandlers) {
    this.id = EntityRevisionId.getInitialInstance(entityId);
    this.interactionHandlers = Set.of(interactionHandlers);
    this.validator = Validation.buildDefaultValidatorFactory().getValidator();
  }

  private EventId createNewEventId() {
    return EventId.builder()
        .entityId(id.getEntityId())
        .parentRevisionId(id.getRevisionId())
        .newRevisionId(new RevisionId())
        .build();
  }

  public <T extends CommandType> EntityRevisionId execute(T command)
      throws EntityCommandFailedException {
    validateCommand(command);
    final ReadModelType readModel = getReadModel();
    return interactionHandlers.stream()
        .filter(handler -> handler.canHandleCommandType(command))
        .findFirst()
        .map(
            handler -> {
              final EventType event =
                  createEventFromCommandWithHandlerFunction(
                      handler.getCommandToEventConverterFunction(createNewEventId()),
                      command,
                      readModel);
              if (event == null) return this.id.getRevisionId();
              final EventType appliedEvent = apply(event);
              if (appliedEvent == null)
                throw new EntityCommandFailedException("Applied event should not be null");
              registerChange(appliedEvent);
              return appliedEvent.getId().getNewRevisionId();
            })
        .map(revisionId -> new EntityRevisionId(this.id.getEntityId(), revisionId))
        .orElse(this.id);
  }

  private void validateCommand(CommandType command) {
    Set<ConstraintViolation<EntityCommand>> violations = validator.validate(command);
    if (!violations.isEmpty()) {
      throw new InvalidCommandException(violations);
    }
  }

  @SuppressWarnings("unchecked")
  private <T extends CommandType, U extends EventType> U createEventFromCommandWithHandlerFunction(
      BiFunction<? extends CommandType, ReadModelType, ? extends EventType> function,
      T command,
      ReadModelType readModel) {
    return ((BiFunction<T, ReadModelType, U>) function).apply(command, readModel);
  }

  @SuppressWarnings("unchecked")
  private <T extends EventType> T applyEventToEntityStateWithHandlerFunction(
      BiFunction<? extends EventType, MutableStateType, ? extends EventType> function,
      T event,
      MutableStateType mutableState) {
    return ((BiFunction<T, MutableStateType, T>) function).apply(event, mutableState);
  }

  protected EventType apply(EventType event) {
    log.info(() -> "applying event " + event);
    final MutableStateType state = getMutableState();
    return interactionHandlers.stream()
        .filter(handler -> handler.canHandleEventType(event))
        .findFirst()
        .map(
            handler ->
                applyEventToEntityStateWithHandlerFunction(
                    handler.getApplyEventFunction(), event, state))
        .map(
            appliedEvent -> {
              this.id =
                  new EntityRevisionId(
                      event.getId().getEntityId(), event.getId().getNewRevisionId());
              afterWithEntityEvent();
              return appliedEvent;
            })
        .orElse(null);
  }

  protected abstract MutableStateType getMutableState();

  protected void afterWithEntityEvent() {}

  private void registerChange(final EventType event) {
    log.info(() -> "registering event with the change listener: " + event);
    if (changeListener != null) changeListener.onChange(event);
  }

  public void setChangeListener(EntityChangeListener<EventType> changeListener) {
    this.changeListener = changeListener;
  }

  @Override
  public EntityRevisionId getId() {
    return id;
  }
}
