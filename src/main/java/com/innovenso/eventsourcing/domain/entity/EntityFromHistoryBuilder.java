package com.innovenso.eventsourcing.domain.entity;

import com.innovenso.eventsourcing.api.command.EntityCommand;
import com.innovenso.eventsourcing.api.entity.EntityMutableState;
import com.innovenso.eventsourcing.api.entity.EntityReadModel;
import com.innovenso.eventsourcing.api.event.EntityEvent;
import com.innovenso.eventsourcing.api.id.RevisionId;
import java.util.function.Supplier;
import lombok.NonNull;
import lombok.extern.java.Log;

@Log
public class EntityFromHistoryBuilder<
    T extends AbstractEntity<C, E, ? extends EntityReadModel, ? extends EntityMutableState>,
    C extends EntityCommand,
    E extends EntityEvent> {
  private final EntityHistory<E> entityHistory;

  public EntityFromHistoryBuilder(@NonNull final EntityHistory<E> entityHistory) {
    this.entityHistory = entityHistory;
  }

  public T build(@NonNull final Supplier<T> entitySupplier, @NonNull final RevisionId revisionId) {
    T entity = entitySupplier.get();
    log.fine(
        () ->
            "loading entity of type "
                + entity.getClass().getName()
                + " with revision "
                + revisionId);
    entityHistory
        .getSnapshot(revisionId)
        .forEach(
            entityEvent -> {
              log.fine(() -> "applying event " + entityEvent);
              entity.apply(entityEvent);
            });
    return entity;
  }
}
