package com.innovenso.eventsourcing.domain.repository;

import com.innovenso.eventsourcing.api.command.EntityCommand;
import com.innovenso.eventsourcing.api.entity.EntityMutableState;
import com.innovenso.eventsourcing.api.entity.EntityReadModel;
import com.innovenso.eventsourcing.api.event.EntityEvent;
import com.innovenso.eventsourcing.api.id.EntityId;
import com.innovenso.eventsourcing.api.id.EntityRevisionId;
import com.innovenso.eventsourcing.api.id.EventId;
import com.innovenso.eventsourcing.api.id.RevisionId;
import com.innovenso.eventsourcing.domain.entity.AbstractEntity;
import com.innovenso.eventsourcing.domain.entity.EntityFromHistoryBuilder;
import com.innovenso.eventsourcing.domain.entity.EntityHistory;
import com.innovenso.eventsourcing.domain.event.EntityChangeListener;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public interface EntityDomainRepository<
        EntityType extends AbstractEntity<CommandType, EventType, ReadModelType, StateType>,
        CommandType extends EntityCommand,
        EventType extends EntityEvent,
        ReadModelType extends EntityReadModel,
        StateType extends EntityMutableState>
    extends EntityChangeListener<EventType> {

  EntityHistory<EventType> getEntityHistory(EntityId entityId);

  default EntityType getEntity(final EntityRevisionId entityRevisionId) {
    EntityType entity =
        new EntityFromHistoryBuilder<EntityType, CommandType, EventType>(
                getEntityHistory(entityRevisionId.getEntityId()))
            .build(
                getNewInstanceSupplier(entityRevisionId.getEntityId()),
                entityRevisionId.getRevisionId());
    entity.setChangeListener(this);
    return entity;
  }

  default EntityType getLatestEntityRevision(final EntityId entityId) {
    return getEntity(getLatestRevisionId(entityId));
  }

  default EventType getEntityEvent(final EntityId entityId, final RevisionId revisionId) {
    return getEntityHistory(entityId).getEvent(revisionId);
  }

  default EntityRevisionId getLatestRevisionId(final EntityId entityId) {
    return new EntityRevisionId(entityId, getEntityHistory(entityId).getLatestRevisionId());
  }

  default ReadModelType getReadModel(final EntityRevisionId entityRevisionId) {
    return getEntity(entityRevisionId).getReadModel();
  }

  default EntityType createEntity() {
    EntityType entity = getNewInstanceSupplier().get();
    entity.setChangeListener(this);
    return entity;
  }

  default EntityType createEntity(EntityId entityId) {
    EntityType entity = getNewInstanceSupplier(entityId).get();
    entity.setChangeListener(this);
    return entity;
  }

  default List<RevisionId> listRevisions(EntityId entityId) {
    return getEntityHistory(entityId).getAllEvents().stream()
        .map(EntityEvent::getId)
        .map(EventId::getNewRevisionId)
        .collect(Collectors.toList());
  }

  List<EntityId> listEntities();

  Supplier<EntityType> getNewInstanceSupplier();

  Supplier<EntityType> getNewInstanceSupplier(EntityId entityId);

  EntityType reset(EntityId entityId);
}
