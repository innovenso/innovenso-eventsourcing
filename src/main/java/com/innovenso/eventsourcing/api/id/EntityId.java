package com.innovenso.eventsourcing.api.id;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.NonNull;

public class EntityId extends AbstractId {

  @JsonCreator
  public EntityId(@NonNull @JsonProperty("value") String value) {
    super(value);
  }

  public EntityId() {
    super();
  }

  @Override
  public String toString() {
    return "EntityId{" + "value='" + value + '\'' + '}';
  }
}
