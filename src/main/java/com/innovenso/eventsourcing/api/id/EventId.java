package com.innovenso.eventsourcing.api.id;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder(builderClassName = "EventIdBuilder")
@JsonDeserialize(builder = EventId.EventIdBuilder.class)
public class EventId {
  @NonNull private final EntityId entityId;
  @NonNull private final RevisionId parentRevisionId;
  @NonNull private final RevisionId newRevisionId;

  @JsonPOJOBuilder(withPrefix = "")
  public static class EventIdBuilder {}

  @JsonIgnore
  public boolean hasParent() {
    return parentRevisionId != null && !entityId.getValue().equals(parentRevisionId.getValue());
  }
}
