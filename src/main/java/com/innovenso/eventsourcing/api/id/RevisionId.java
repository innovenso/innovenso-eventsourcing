package com.innovenso.eventsourcing.api.id;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Strings;
import java.util.Date;
import java.util.Objects;
import lombok.NonNull;

public class RevisionId extends AbstractId {
  private final long timestamp;

  @JsonCreator
  public RevisionId(
      @NonNull @JsonProperty("value") String value, @JsonProperty("timestamp") long timestamp) {
    super(value);
    this.timestamp = timestamp;
  }

  public RevisionId(String value) {
    super(value);
    this.timestamp = new Date().getTime();
  }

  public RevisionId() {
    super();
    this.timestamp = new Date().getTime();
  }

  @Override
  public String toString() {
    return "RevisionId{" + "timestamp=" + timestamp + ", value='" + value + '\'' + '}';
  }

  public long getTimestamp() {
    return timestamp;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    if (!super.equals(o)) return false;
    RevisionId that = (RevisionId) o;
    return timestamp == that.timestamp;
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), timestamp);
  }

  public String encode() {
    return value + ":" + String.valueOf(timestamp);
  }

  public static RevisionId decode(@NonNull final String encodedValue) {
    String[] split = encodedValue.split(":");
    if (split.length != 2 || Strings.isNullOrEmpty(split[0]) || Strings.isNullOrEmpty(split[1]))
      throw new IllegalArgumentException("encoded value is not a revision id: " + encodedValue);
    try {
      return new RevisionId(split[0], Long.parseLong(split[1]));
    } catch (NumberFormatException nfe) {
      throw new IllegalArgumentException("encoded value is not a revision id: " + encodedValue);
    }
  }
}
