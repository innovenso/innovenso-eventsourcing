package com.innovenso.eventsourcing.api.id;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.google.common.base.Strings;
import java.util.Objects;
import lombok.NonNull;

public class EntityRevisionId {
  private final RevisionId revisionId;
  private final EntityId entityId;

  @JsonCreator
  public EntityRevisionId(@NonNull final EntityId entityId, @NonNull final RevisionId revisionId) {
    this.entityId = entityId;
    this.revisionId = revisionId;
  }

  public static EntityRevisionId getInitialInstance(EntityId entityId) {
    return new EntityRevisionId(entityId, new RevisionId(entityId.getValue(), 0));
  }

  public RevisionId getRevisionId() {
    return revisionId;
  }

  public EntityId getEntityId() {
    return entityId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    EntityRevisionId that = (EntityRevisionId) o;
    return revisionId.equals(that.revisionId) && entityId.equals(that.entityId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(revisionId, entityId);
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("EntityRevisionId{");
    sb.append("revisionId=").append(revisionId);
    sb.append(", entityId=").append(entityId);
    sb.append('}');
    return sb.toString();
  }

  public String encode() {
    return entityId.value + ":" + revisionId.encode();
  }

  public static EntityRevisionId decode(@NonNull final String encodedValue) {
    String[] split = encodedValue.split(":", 2);
    if (split.length != 2 || Strings.isNullOrEmpty(split[0]) || Strings.isNullOrEmpty(split[1]))
      throw new IllegalArgumentException(
          "encoded value is not an entity revision id: " + encodedValue);
    return new EntityRevisionId(new EntityId(split[0]), RevisionId.decode(split[1]));
  }
}
