package com.innovenso.eventsourcing.api.id;

import java.util.Objects;
import java.util.UUID;

public abstract class AbstractId {
  public final String value;

  public AbstractId(String value) {
    if (value == null || value.length() <= 0)
      throw new IllegalArgumentException("value is required");
    this.value = value;
  }

  public AbstractId() {
    this.value = UUID.randomUUID().toString();
  }

  public String getValue() {
    return value;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    AbstractId that = (AbstractId) o;
    return Objects.equals(value, that.value);
  }

  @Override
  public int hashCode() {
    return Objects.hash(value);
  }
}
