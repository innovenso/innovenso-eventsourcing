package com.innovenso.eventsourcing.api.event;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.innovenso.eventsourcing.api.id.EventId;

@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.PROPERTY,
    property = "eventType")
public interface EntityEvent {
  EventId getId();
}
