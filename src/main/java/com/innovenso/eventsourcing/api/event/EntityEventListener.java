package com.innovenso.eventsourcing.api.event;

public interface EntityEventListener<T extends EntityEvent> {
  void onEvent(T event);
}
