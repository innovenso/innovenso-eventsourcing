package com.innovenso.eventsourcing.api.exception;

public class EntityCommandFailedException extends RuntimeException {
  public EntityCommandFailedException() {
    super();
  }

  public EntityCommandFailedException(String message) {
    super(message);
  }

  public EntityCommandFailedException(String message, Throwable cause) {
    super(message, cause);
  }

  public EntityCommandFailedException(Throwable cause) {
    super(cause);
  }
}
