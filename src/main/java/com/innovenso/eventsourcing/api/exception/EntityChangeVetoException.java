package com.innovenso.eventsourcing.api.exception;

public class EntityChangeVetoException extends RuntimeException {
  public EntityChangeVetoException() {
    super();
  }

  public EntityChangeVetoException(String message) {
    super(message);
  }

  public EntityChangeVetoException(String message, Throwable cause) {
    super(message, cause);
  }

  public EntityChangeVetoException(Throwable cause) {
    super(cause);
  }
}
