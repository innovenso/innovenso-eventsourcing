package com.innovenso.eventsourcing.api.exception;

import com.innovenso.eventsourcing.api.command.EntityCommand;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import javax.validation.ConstraintViolation;
import lombok.NonNull;

public class InvalidCommandException extends RuntimeException {
  private final Set<ConstraintViolation<EntityCommand>> violations;

  public InvalidCommandException(
      @NonNull final Set<ConstraintViolation<EntityCommand>> violations) {
    this.violations = violations;
  }

  public Set<ConstraintViolation<EntityCommand>> getViolations() {
    return Set.copyOf(violations);
  }

  public Set<ConstraintViolation<EntityCommand>> getViolations(@NonNull final String propertyPath) {
    return violations.stream()
        .filter(violation -> Objects.equals(violation.getPropertyPath().toString(), propertyPath))
        .collect(Collectors.toSet());
  }
}
