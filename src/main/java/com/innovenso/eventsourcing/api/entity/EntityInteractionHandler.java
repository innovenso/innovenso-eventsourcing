package com.innovenso.eventsourcing.api.entity;

import com.innovenso.eventsourcing.api.command.EntityCommand;
import com.innovenso.eventsourcing.api.event.EntityEvent;
import com.innovenso.eventsourcing.api.id.EventId;
import java.util.function.BiFunction;

public interface EntityInteractionHandler<
    CommandType extends EntityCommand,
    EventType extends EntityEvent,
    ReadModelType extends EntityReadModel,
    MutableStateType extends EntityMutableState> {
  <T extends EntityCommand> boolean canHandleCommandType(T command);

  <T extends EntityEvent> boolean canHandleEventType(T event);

  BiFunction<CommandType, ReadModelType, EventType> getCommandToEventConverterFunction(
      final EventId eventId);

  BiFunction<EventType, MutableStateType, EventType> getApplyEventFunction();
}
