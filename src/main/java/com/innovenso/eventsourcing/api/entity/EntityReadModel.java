package com.innovenso.eventsourcing.api.entity;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.PROPERTY,
    property = "readModelType")
public interface EntityReadModel {}
