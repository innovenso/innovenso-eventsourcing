package com.innovenso.eventsourcing.api.entity;

import com.innovenso.eventsourcing.api.id.EntityRevisionId;

public interface HasEntityRevision {
  EntityRevisionId getId();
}
