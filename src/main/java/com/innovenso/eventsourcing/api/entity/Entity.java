package com.innovenso.eventsourcing.api.entity;

public interface Entity<
        ReadModelType extends EntityReadModel, MutableStateType extends EntityMutableState>
    extends HasEntityRevision {
  ReadModelType getReadModel();
}
