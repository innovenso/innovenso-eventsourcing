package com.innovenso.eventsourcing.api.repository;

import com.innovenso.eventsourcing.api.entity.Entity;
import com.innovenso.eventsourcing.api.entity.EntityMutableState;
import com.innovenso.eventsourcing.api.entity.EntityReadModel;
import com.innovenso.eventsourcing.api.id.EntityId;
import com.innovenso.eventsourcing.api.id.EntityRevisionId;

public interface EntityRepository<
    T extends Entity<? extends EntityReadModel, ? extends EntityMutableState>> {
  T getRevision(EntityRevisionId entityRevisionId);

  T getLatestRevision(EntityId entityId);
}
