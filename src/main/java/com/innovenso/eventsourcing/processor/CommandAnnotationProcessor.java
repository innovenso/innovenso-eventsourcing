package com.innovenso.eventsourcing.processor;

import com.google.auto.service.AutoService;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import javax.tools.Diagnostic;

@SupportedAnnotationTypes("com.innovenso.eventsourcing.api.command.Command")
@SupportedSourceVersion(SourceVersion.RELEASE_8)
@AutoService(Processor.class)
public class CommandAnnotationProcessor extends AbstractProcessor {

  @Override
  public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
    annotations.stream()
        .flatMap(
            annotation ->
                roundEnv.getElementsAnnotatedWith(annotation).stream()
                    .peek(
                        element ->
                            processingEnv
                                .getMessager()
                                .printMessage(Diagnostic.Kind.NOTE, "processing command", element))
                    .filter(element -> element instanceof TypeElement)
                    .map(element -> (TypeElement) element))
        .collect(Collectors.groupingBy(TypeElement::getSuperclass))
        .forEach(this::createJsonCommandMapper);
    return false;
  }

  private void createJsonCommandMapper(
      final TypeMirror superClass, final List<TypeElement> commandClasses) {}
}
