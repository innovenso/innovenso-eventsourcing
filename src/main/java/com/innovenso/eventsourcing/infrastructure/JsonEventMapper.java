package com.innovenso.eventsourcing.infrastructure;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.databind.jsontype.NamedType;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.innovenso.eventsourcing.api.event.EntityEvent;
import java.util.HashSet;
import java.util.Set;
import lombok.NonNull;

public abstract class JsonEventMapper<EventType extends EntityEvent> {
  private final Class<EventType> eventClass;
  private final Set<NamedType> namedTypes;

  public JsonEventMapper(@NonNull Class<EventType> eventClass) {
    this.eventClass = eventClass;
    this.namedTypes = new HashSet<>();
  }

  protected void registerSubType(Class<? extends EntityEvent> subTypeClass, String subTypeName) {
    if (eventClass.isAssignableFrom(subTypeClass)) {
      this.namedTypes.add(new NamedType(subTypeClass, subTypeName));
    }
  }

  public ObjectMapper getObjectMapper() {
    return JsonMapper.builder()
        .registerSubtypes(namedTypes.toArray(new NamedType[0]))
        .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
        .build()
        .registerModule(new JavaTimeModule());
  }

  public void decorateObjectMapper(final @NonNull ObjectMapper in) {
    in.registerSubtypes(namedTypes.toArray(new NamedType[0]));
    in.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    in.registerModule(new JavaTimeModule());
  }
}
