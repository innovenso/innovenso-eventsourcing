package com.innovenso.eventsourcing.infrastructure.repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.innovenso.eventsourcing.api.command.EntityCommand;
import com.innovenso.eventsourcing.api.entity.EntityMutableState;
import com.innovenso.eventsourcing.api.entity.EntityReadModel;
import com.innovenso.eventsourcing.api.event.EntityEvent;
import com.innovenso.eventsourcing.api.exception.EntityChangeVetoException;
import com.innovenso.eventsourcing.api.id.EntityId;
import com.innovenso.eventsourcing.domain.entity.AbstractEntity;
import com.innovenso.eventsourcing.domain.entity.EntityHistory;
import com.innovenso.eventsourcing.domain.event.EntityChangeListener;
import com.innovenso.eventsourcing.domain.event.EntityEventPublisher;
import com.innovenso.eventsourcing.domain.repository.EntityDomainRepository;
import com.innovenso.eventsourcing.infrastructure.JsonEventMapper;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import lombok.NonNull;
import lombok.extern.java.Log;
import org.apache.commons.io.FileUtils;

@Log
public abstract class EntityDomainFileSystemRepository<
        EntityType extends AbstractEntity<CommandType, EventType, ReadModelType, StateType>,
        CommandType extends EntityCommand,
        EventType extends EntityEvent,
        ReadModelType extends EntityReadModel,
        StateType extends EntityMutableState>
    implements EntityDomainRepository<EntityType, CommandType, EventType, ReadModelType, StateType>,
        EntityChangeListener<EventType> {

  private File repositoryRoot;
  private final Class<EventType> eventClass;
  private final Class<EntityType> entityClass;
  private final Map<Class<? extends EventType>, String> eventTypes = new HashMap<>();
  private final EntityEventPublisher<EventType> publisher;
  private final JsonEventMapper<EventType> jsonEventMapper;

  public EntityDomainFileSystemRepository(
      @NonNull final File repositoryDirectory,
      @NonNull final JsonEventMapper<EventType> jsonEventMapper,
      @NonNull Class<EntityType> entityClass,
      @NonNull Class<EventType> eventClass) {
    this.eventClass = eventClass;
    this.entityClass = entityClass;
    this.publisher = new EntityEventPublisher<>();
    this.jsonEventMapper = jsonEventMapper;
    setRepositoryRoot(repositoryDirectory);
  }

  private File getEntityDirectory(@NonNull EntityId entityId) {
    final File entityDirectory = new File(repositoryRoot, entityId.value);
    if (!entityDirectory.exists()) entityDirectory.mkdirs();
    return entityDirectory;
  }

  @Override
  public EntityHistory<EventType> getEntityHistory(@NonNull EntityId entityId) {
    log.fine(() -> "loading entity history for entity " + entityId);
    EntityHistory<EventType> entityHistory = new EntityHistory<>(entityId);
    File entityDirectory = getEntityDirectory(entityId);
    ObjectMapper objectMapper = jsonEventMapper.getObjectMapper();
    if (entityDirectory.exists() && entityDirectory.isDirectory()) {
      File[] files = entityDirectory.listFiles();
      if (files != null) {
        Arrays.stream(files)
            .forEach(
                eventFile -> {
                  try {
                    log.fine(() -> "reading event from file " + eventFile.getAbsolutePath());
                    EventType event = objectMapper.readValue(eventFile, eventClass);
                    log.fine(() -> "event: " + event);
                    entityHistory.addEvent(event);
                  } catch (IOException e) {
                    log.warning(e::toString);
                  }
                });
      }
    }
    return entityHistory;
  }

  @Override
  public List<EntityId> listEntities() {
    if (repositoryRoot.exists() && repositoryRoot.isDirectory()) {
      File[] files = repositoryRoot.listFiles();
      if (files != null) {
        return Arrays.stream(files)
            .map(file -> new EntityId(file.getName()))
            .collect(Collectors.toList());
      }
    }
    return Collections.emptyList();
  }

  @Override
  public EntityType reset(@NonNull final EntityId entityId) {
    File entityDirectory = getEntityDirectory(entityId);
    try {
      FileUtils.cleanDirectory(entityDirectory);
    } catch (IOException e) {
      log.warning(e::toString);
    }
    return getLatestEntityRevision(entityId);
  }

  @Override
  public void onChange(EventType event) throws EntityChangeVetoException {
    File entityDirectory = getEntityDirectory(event.getId().getEntityId());
    entityDirectory.mkdirs();

    try {
      File eventFile = new File(entityDirectory, event.getId().getNewRevisionId().value);
      jsonEventMapper.getObjectMapper().writeValue(eventFile, event);
    } catch (JsonProcessingException jpe) {
      throw new EntityChangeVetoException("could not parse events");
    } catch (IOException e) {
      throw new EntityChangeVetoException("could not persist events");
    }
    publisher.publish(event);
  }

  public File getRepositoryRoot() {
    return repositoryRoot;
  }

  public void setRepositoryRoot(@NonNull final File repositoryRoot) {
    this.repositoryRoot = repositoryRoot;
    this.repositoryRoot.mkdirs();
  }

  public EntityEventPublisher<EventType> getEventPublisher() {
    return publisher;
  }
}
