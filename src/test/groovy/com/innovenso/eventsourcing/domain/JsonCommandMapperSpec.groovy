package com.innovenso.eventsourcing.domain

import com.fasterxml.jackson.databind.ObjectMapper
import com.innovenso.eventsourcing.infrastructure.JsonCommandMapper
import com.innovenso.eventsourcing.profile.api.ProfileCommand
import com.innovenso.eventsourcing.profile.domain.address.UpdateProfileAddressCommand
import com.innovenso.eventsourcing.profile.domain.name.UpdateProfileNameCommand
import com.innovenso.eventsourcing.profile.infrastructure.ProfileJsonCommandMapper
import spock.lang.Specification

class JsonCommandMapperSpec extends Specification {
	private ObjectMapper objectMapper
	private JsonCommandMapper<ProfileCommand> commandMapper

	def setup() {
		objectMapper = new ObjectMapper();
		commandMapper = new ProfileJsonCommandMapper<>(ProfileCommand.class)
	}

	def "commands can be serialized to JSON"() {
		when:
		String name = objectMapper.writeValueAsString(UpdateProfileNameCommand.builder().firstName("Jurgen").lastName("Lust").build())
		String address = objectMapper.writeValueAsString(UpdateProfileAddressCommand.builder().street("Houtbriel").city("Gent").postalCode("9000").number("26").build())
		then:
		println name
		name
		println address
		address
	}

	def "commands can be deserialized from JSON"() {
		when:
		def json = '{"commandType":"updateNameCommand","firstName":"Jurgen","lastName":"Lust"}'
		UpdateProfileNameCommand command = objectMapper.readValue(json, UpdateProfileNameCommand.class)
		then:
		command.firstName == "Jurgen"
		command.lastName == "Lust"
	}
}
