package com.innovenso.eventsourcing.domain

import com.innovenso.eventsourcing.api.exception.InvalidCommandException
import com.innovenso.eventsourcing.api.id.EntityId
import com.innovenso.eventsourcing.profile.domain.ProfileEntityImpl
import com.innovenso.eventsourcing.profile.domain.address.UpdateProfileAddressCommand
import com.innovenso.eventsourcing.profile.domain.name.UpdateProfileNameCommand
import com.innovenso.eventsourcing.profile.infrastructure.ProfileEntityFileSystemRepository
import com.innovenso.eventsourcing.profile.infrastructure.ProfileEntityRepository
import spock.lang.Specification

class EntitySpec extends Specification {

	def "executing a command results in an updated state"() {
		given:
		ProfileEntityImpl profileEntity = new ProfileEntityImpl()
		when:
		profileEntity.execute(UpdateProfileNameCommand.builder()
				.firstName("Jurgen")
				.lastName("Lust")
				.build())
		then:
		profileEntity.readModel.firstName == "Jurgen"
		profileEntity.readModel.lastName == "Lust"
	}

	def "executing a command results in an event being saved to the repository"() {
		given:
		ProfileEntityRepository repository = Mock(ProfileEntityRepository.class)
		ProfileEntityImpl profileEntity = new ProfileEntityImpl()
		profileEntity.setChangeListener(repository)
		when:
		profileEntity.execute(UpdateProfileNameCommand.builder().firstName("Arthur").lastName("Lust").build())
		then:
		1 * repository.onChange({ event -> event.firstName == "Arthur" && event.lastName == "Lust" })
	}

	def "when a interaction handler returns a null event for a command no changes are made"() {
		given:
		ProfileEntityRepository repository = Mock(ProfileEntityRepository.class)
		ProfileEntityImpl profileEntity = new ProfileEntityImpl()
		profileEntity.setChangeListener(repository)
		when:
		2.times {
			profileEntity.execute(UpdateProfileNameCommand.builder().firstName("Arthur").lastName("Lust").build())
		}
		then:
		1 * repository.onChange({event -> event.firstName == "Arthur" && event.lastName == "Lust" })
	}

	def "executing multiple commands result in multiple repository changes"() {
		given:
		ProfileEntityRepository repository = Mock(ProfileEntityRepository.class)
		ProfileEntityImpl profileEntity = new ProfileEntityImpl()
		profileEntity.setChangeListener(repository)
		int numberOfCommands = 10
		when:
		numberOfCommands.times {
			profileEntity.execute(UpdateProfileNameCommand.builder().firstName("Arthur " + it).lastName("Lust").build())
		}
		then:
		numberOfCommands * repository.onChange({event -> event.lastName == "Lust" })
		profileEntity.readModel.firstName == "Arthur " + (numberOfCommands - 1)
	}

	def "events are properly persisted and load to and from the repository"() {
		given:
		ProfileEntityFileSystemRepository repository = new ProfileEntityFileSystemRepository()
		ProfileEntityImpl profile = repository.createEntity(new EntityId("innovenso_test"))
		profile.execute(UpdateProfileNameCommand.builder().firstName("Jurgen").lastName("Lust").build())
		profile.execute(UpdateProfileAddressCommand.builder().street("Lievestraat").number("16").postalCode("9000").city("Gent").build())
		when:
		ProfileEntityImpl loadedProfile = repository.getLatestEntityRevision(new EntityId("innovenso_test"))

		then:
		loadedProfile.readModel.firstName == "Jurgen"
		loadedProfile.readModel.lastName == "Lust"
		with (loadedProfile.readModel.address) {
			it.street == "Lievestraat"
			it.number == "16"
			it.postalCode == "9000"
			it.city == "Gent"
		}
	}

	def "commands are validated before execution"() {
		given:
		ProfileEntityImpl profileEntity = new ProfileEntityImpl()
		when:
		profileEntity.execute(command)
		then:
		InvalidCommandException exception = thrown()
		exception.violations.size() == numberOfViolations
		exception.violations.each {println it.propertyPath}
		where:
		command | numberOfViolations
		UpdateProfileNameCommand.builder().build() | 2
		UpdateProfileNameCommand.builder().firstName("Jurgen").build() | 1
		UpdateProfileNameCommand.builder().firstName("").build() | 2
		UpdateProfileAddressCommand.builder().build() | 4
	}
}
