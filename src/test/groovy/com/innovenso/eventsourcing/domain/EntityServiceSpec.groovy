package com.innovenso.eventsourcing.domain

import com.innovenso.eventsourcing.api.id.EntityId
import com.innovenso.eventsourcing.api.id.EntityRevisionId
import com.innovenso.eventsourcing.api.id.RevisionId
import com.innovenso.eventsourcing.profile.api.ProfileCommand
import com.innovenso.eventsourcing.profile.domain.ProfileEntityImpl
import com.innovenso.eventsourcing.profile.domain.name.UpdateProfileNameCommand
import com.innovenso.eventsourcing.profile.infrastructure.ProfileEntityFileSystemRepository
import com.innovenso.eventsourcing.profile.infrastructure.ProfileEntityRepository
import com.innovenso.eventsourcing.profile.infrastructure.ProfileEntityService
import spock.lang.Specification

class EntityServiceSpec extends Specification {

	def "creating a new entity results in a new entity id"() {
		given:
		def repository = Stub(ProfileEntityRepository) {
			createEntity() >> new ProfileEntityImpl(new EntityId("test_entity"))
		}
		def service = new ProfileEntityService(repository)

		when:
		def id = service.create()
		then:
		println id
		id == "test_entity"
	}

	def "a command can be executed on a new entity"() {
		given:
		def repository = new ProfileEntityFileSystemRepository()
		def service = new ProfileEntityService(repository)
		when:
		def id = service.create()
		def revisionId = service.execute(id, UpdateProfileNameCommand.builder().firstName("Jurgen").lastName("Lust").build())
		def readModel = service.load(id, revisionId)
		then:
		repository.getLatestRevisionId(new EntityId(id)).revisionId.encode() == revisionId
		service.getLatestRevisionId(id) == revisionId
		repository.getReadModel(new EntityRevisionId(new EntityId(id), RevisionId.decode(revisionId))).firstName == "Jurgen"
		readModel.firstName == "Jurgen"
		readModel.lastName == "Lust"
	}
}
