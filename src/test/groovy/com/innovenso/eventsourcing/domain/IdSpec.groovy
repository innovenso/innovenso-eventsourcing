package com.innovenso.eventsourcing.domain

import com.innovenso.eventsourcing.api.id.EntityId
import com.innovenso.eventsourcing.api.id.EntityRevisionId
import com.innovenso.eventsourcing.api.id.RevisionId
import spock.lang.Specification

class IdSpec extends Specification {
	def "revision id can be decoded from an encoded string"() {
		when:
		RevisionId revisionId = RevisionId.decode("abc:123")
		then:
		revisionId.value == "abc"
		revisionId.timestamp == 123
	}

	def "revision id can be encoded to a string"() {
		when:
		def encoded = new RevisionId("xyz", 456).encode()
		then:
		encoded == "xyz:456"
	}

	def "revision id that is encoded can be decoded again"() {
		when:
		RevisionId original = new RevisionId("abcdef", 123456)
		RevisionId cloned = RevisionId.decode(original.encode())
		then:
		original == cloned
		cloned.timestamp == 123456
		cloned.value == "abcdef"
	}

	def "entity revision id can be encoded and decoded again"() {
		when:
		EntityRevisionId original = new EntityRevisionId(new EntityId("entityA"), new RevisionId("abc", 123))
		String encoded = original.encode()
		println encoded
		EntityRevisionId cloned = EntityRevisionId.decode(encoded)
		then:
		original == cloned
		encoded == "entityA:abc:123"
		cloned.entityId.value == "entityA"
		cloned.revisionId.value == "abc"
		cloned.revisionId.timestamp == 123
	}

	def "invalid revision id string results in IllegalArgumentException"(String encodedValue) {
		when:
		RevisionId.decode(encodedValue)
		then:
		thrown(IllegalArgumentException)
		where:
		encodedValue | _
		"abc" | _
		"abc:def" | _
		"abc:123:456" | _
		"abc:def:123" | _
		"" | _
		"123" | _
		":" | _
		":123" | _
		"abc:" | _
	}

	def "invalid entity revision id string results in IllegalArgumentException"(String encodedValue) {
		when:
		EntityRevisionId.decode(encodedValue)
		then:
		thrown(IllegalArgumentException)
		where:
		encodedValue | _
		"abc" | _
		"abc:def" | _
		"" | _
		"123" | _
		":" | _
		"::" | _
		":123:" | _
		"abc::" | _
		"abc::123" | _
		"::123" | _
		"::abc" | _
	}
}
