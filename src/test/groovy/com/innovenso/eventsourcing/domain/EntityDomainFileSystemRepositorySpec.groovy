package com.innovenso.eventsourcing.domain

import com.innovenso.eventsourcing.profile.api.ProfileEntity
import com.innovenso.eventsourcing.profile.domain.ProfileEntityImpl
import com.innovenso.eventsourcing.profile.domain.name.UpdateProfileNameCommand
import com.innovenso.eventsourcing.profile.infrastructure.ProfileEntityFileSystemRepository
import spock.lang.Specification

class EntityDomainFileSystemRepositorySpec extends Specification {
	def "latest revision id is always the same if nothing changes"() {
		when:
		def repository = new ProfileEntityFileSystemRepository()
		ProfileEntityImpl entity = repository.createEntity()
		def latestRevision = repository.getLatestRevisionId(entity.id.entityId)
		then:
		10.times {
			with (repository.getLatestRevisionId(entity.id.entityId)) {
				println it
				it == latestRevision
			}
		}
	}
}
