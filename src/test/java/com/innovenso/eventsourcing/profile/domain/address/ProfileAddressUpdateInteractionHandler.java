package com.innovenso.eventsourcing.profile.domain.address;

import com.innovenso.eventsourcing.api.command.EntityCommand;
import com.innovenso.eventsourcing.api.entity.EntityInteractionHandler;
import com.innovenso.eventsourcing.api.event.EntityEvent;
import com.innovenso.eventsourcing.api.id.EventId;
import com.innovenso.eventsourcing.profile.api.Address;
import com.innovenso.eventsourcing.profile.api.ProfileEntityReadModel;
import com.innovenso.eventsourcing.profile.api.ProfileEntityState;
import java.util.function.BiFunction;

public class ProfileAddressUpdateInteractionHandler
    implements EntityInteractionHandler<
        UpdateProfileAddressCommand,
        ProfileAddressUpdatedEvent,
        ProfileEntityReadModel,
        ProfileEntityState> {
  @Override
  public <T extends EntityCommand> boolean canHandleCommandType(T command) {
    return command instanceof UpdateProfileAddressCommand;
  }

  @Override
  public <T extends EntityEvent> boolean canHandleEventType(T event) {
    return event instanceof ProfileAddressUpdatedEvent;
  }

  @Override
  public BiFunction<UpdateProfileAddressCommand, ProfileEntityReadModel, ProfileAddressUpdatedEvent>
      getCommandToEventConverterFunction(EventId eventId) {
    return (command, readModel) ->
        ProfileAddressUpdatedEvent.builder()
            .id(eventId)
            .street(command.getStreet())
            .number(command.getNumber())
            .postalCode(command.getPostalCode())
            .city(command.getCity())
            .build();
  }

  @Override
  public BiFunction<ProfileAddressUpdatedEvent, ProfileEntityState, ProfileAddressUpdatedEvent>
      getApplyEventFunction() {
    return (event, state) -> {
      state.setAddress(
          Address.builder()
              .street(event.getStreet())
              .city(event.getCity())
              .number(event.getNumber())
              .postalCode(event.getPostalCode())
              .build());
      return event;
    };
  }
}
