package com.innovenso.eventsourcing.profile.domain.address;

import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.command.Command;
import com.innovenso.eventsourcing.profile.api.ProfileCommand;
import javax.validation.constraints.NotBlank;
import lombok.Builder;
import lombok.Value;

@Value
@Command
@Builder(builderClassName = "UpdateProfileAddressCommandBuilder")
@JsonDeserialize(builder = UpdateProfileAddressCommand.UpdateProfileAddressCommandBuilder.class)
@JsonTypeName("updateAddressCommand")
public class UpdateProfileAddressCommand implements ProfileCommand {
  @NotBlank String street;
  @NotBlank String number;
  @NotBlank String postalCode;
  @NotBlank String city;

  @JsonPOJOBuilder(withPrefix = "")
  public static class UpdateProfileAddressCommandBuilder {}
}
