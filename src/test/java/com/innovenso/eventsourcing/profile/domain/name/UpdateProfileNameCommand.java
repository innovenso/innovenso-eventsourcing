package com.innovenso.eventsourcing.profile.domain.name;

import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.command.Command;
import com.innovenso.eventsourcing.profile.api.ProfileCommand;
import javax.validation.constraints.NotBlank;
import lombok.Builder;
import lombok.Value;

@Value
@Builder(builderClassName = "UpdateProfileNameCommandBuilder")
@JsonDeserialize(builder = UpdateProfileNameCommand.UpdateProfileNameCommandBuilder.class)
@Command
@JsonTypeName("updateNameCommand")
public class UpdateProfileNameCommand implements ProfileCommand {
  @NotBlank String firstName;
  @NotBlank String lastName;

  @JsonPOJOBuilder(withPrefix = "")
  public static class UpdateProfileNameCommandBuilder {}
}
