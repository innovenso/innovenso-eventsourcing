package com.innovenso.eventsourcing.profile.domain.name;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.event.Event;
import com.innovenso.eventsourcing.api.id.EventId;
import com.innovenso.eventsourcing.profile.api.ProfileEvent;
import lombok.Builder;
import lombok.Value;

@Value
@Event
@Builder(builderClassName = "ProfileNameUpdatedEventBuilder")
@JsonDeserialize(builder = ProfileNameUpdatedEvent.ProfileNameUpdatedEventBuilder.class)
public class ProfileNameUpdatedEvent implements ProfileEvent {
  EventId id;
  String firstName;
  String lastName;

  @JsonPOJOBuilder(withPrefix = "")
  public static class ProfileNameUpdatedEventBuilder {}
}
