package com.innovenso.eventsourcing.profile.domain.name;

import com.innovenso.eventsourcing.api.command.EntityCommand;
import com.innovenso.eventsourcing.api.entity.EntityInteractionHandler;
import com.innovenso.eventsourcing.api.event.EntityEvent;
import com.innovenso.eventsourcing.api.id.EventId;
import com.innovenso.eventsourcing.profile.api.ProfileEntityReadModel;
import com.innovenso.eventsourcing.profile.api.ProfileEntityState;
import java.util.Objects;
import java.util.function.BiFunction;
import lombok.NonNull;
import lombok.extern.java.Log;

@Log
public class ProfileNameUpdateInteractionHandler
    implements EntityInteractionHandler<
        UpdateProfileNameCommand,
        ProfileNameUpdatedEvent,
        ProfileEntityReadModel,
        ProfileEntityState> {
  private boolean isCommandAllowedToExecute(
      UpdateProfileNameCommand entityCommand, ProfileEntityReadModel entityReadModel) {
    boolean allowed =
        !Objects.equals(entityCommand.getFirstName(), entityReadModel.getFirstName())
            || !Objects.equals(entityCommand.getLastName(), entityReadModel.getLastName());
    log.info(() -> "is command allowed to execute? " + entityCommand + ": " + allowed);
    return allowed;
  }

  @Override
  public <T extends EntityCommand> boolean canHandleCommandType(T command) {
    return command instanceof UpdateProfileNameCommand;
  }

  @Override
  public <T extends EntityEvent> boolean canHandleEventType(T event) {
    return event instanceof ProfileNameUpdatedEvent;
  }

  @Override
  public BiFunction<UpdateProfileNameCommand, ProfileEntityReadModel, ProfileNameUpdatedEvent>
      getCommandToEventConverterFunction(@NonNull final EventId eventId) {
    return (command, readModel) ->
        isCommandAllowedToExecute(command, readModel)
            ? ProfileNameUpdatedEvent.builder()
                .id(eventId)
                .firstName(command.getFirstName())
                .lastName(command.getLastName())
                .build()
            : null;
  }

  @Override
  public BiFunction<ProfileNameUpdatedEvent, ProfileEntityState, ProfileNameUpdatedEvent>
      getApplyEventFunction() {
    return (event, state) -> {
      state.setFirstName(event.getFirstName());
      state.setLastName(event.getLastName());
      return event;
    };
  }
}
