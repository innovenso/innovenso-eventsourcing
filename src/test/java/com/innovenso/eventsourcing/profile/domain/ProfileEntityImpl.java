package com.innovenso.eventsourcing.profile.domain;

import com.innovenso.eventsourcing.api.id.EntityId;
import com.innovenso.eventsourcing.domain.entity.AbstractEntity;
import com.innovenso.eventsourcing.profile.api.*;
import com.innovenso.eventsourcing.profile.domain.address.ProfileAddressUpdateInteractionHandler;
import com.innovenso.eventsourcing.profile.domain.name.ProfileNameUpdateInteractionHandler;
import lombok.Data;
import lombok.NonNull;

public class ProfileEntityImpl
    extends AbstractEntity<
        ProfileCommand, ProfileEvent, ProfileEntityReadModel, ProfileEntityState> {
  private final State state = new State();

  public ProfileEntityImpl() {
    super(new ProfileNameUpdateInteractionHandler(), new ProfileAddressUpdateInteractionHandler());
  }

  public ProfileEntityImpl(@NonNull EntityId entityId) {
    super(
        entityId,
        new ProfileNameUpdateInteractionHandler(),
        new ProfileAddressUpdateInteractionHandler());
  }

  @Override
  public ProfileEntityReadModel getReadModel() {
    return state;
  }

  @Override
  protected ProfileEntityState getMutableState() {
    return state;
  }

  @Data
  public static final class State implements ProfileEntityState {
    private State() {}

    String firstName;
    String lastName;
    Address address;

    @Override
    public String getFullName() {
      return firstName + " " + lastName;
    }
  }
}
