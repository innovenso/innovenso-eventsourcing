package com.innovenso.eventsourcing.profile.domain.address;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.innovenso.eventsourcing.api.event.Event;
import com.innovenso.eventsourcing.api.id.EventId;
import com.innovenso.eventsourcing.profile.api.ProfileEvent;
import lombok.Builder;
import lombok.Value;

@Value
@Event
@Builder(builderClassName = "ProfileAddressUpdatedEventBuilder")
@JsonDeserialize(builder = ProfileAddressUpdatedEvent.ProfileAddressUpdatedEventBuilder.class)
public class ProfileAddressUpdatedEvent implements ProfileEvent {
  EventId id;
  String street;
  String number;
  String postalCode;
  String city;

  @JsonPOJOBuilder(withPrefix = "")
  public static class ProfileAddressUpdatedEventBuilder {}
}
