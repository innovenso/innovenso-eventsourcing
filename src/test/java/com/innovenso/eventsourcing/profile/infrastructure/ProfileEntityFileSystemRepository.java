package com.innovenso.eventsourcing.profile.infrastructure;

import com.innovenso.eventsourcing.infrastructure.repository.EntityDomainFileSystemRepository;
import com.innovenso.eventsourcing.profile.api.ProfileCommand;
import com.innovenso.eventsourcing.profile.api.ProfileEntityReadModel;
import com.innovenso.eventsourcing.profile.api.ProfileEntityState;
import com.innovenso.eventsourcing.profile.api.ProfileEvent;
import com.innovenso.eventsourcing.profile.domain.ProfileEntityImpl;
import java.io.File;

public class ProfileEntityFileSystemRepository
    extends EntityDomainFileSystemRepository<
        ProfileEntityImpl, ProfileCommand, ProfileEvent, ProfileEntityReadModel, ProfileEntityState>
    implements ProfileEntityRepository {
  public ProfileEntityFileSystemRepository() {
    super(
        new File("/tmp/innovenso/eventsourcing"),
        new ProfileJsonEventMapper<>(ProfileEvent.class),
        ProfileEntityImpl.class,
        ProfileEvent.class);
  }
}
