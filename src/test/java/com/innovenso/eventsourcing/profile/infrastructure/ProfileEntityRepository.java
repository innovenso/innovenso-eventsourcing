package com.innovenso.eventsourcing.profile.infrastructure;

import com.innovenso.eventsourcing.api.id.EntityId;
import com.innovenso.eventsourcing.domain.repository.EntityDomainRepository;
import com.innovenso.eventsourcing.profile.api.ProfileCommand;
import com.innovenso.eventsourcing.profile.api.ProfileEntityReadModel;
import com.innovenso.eventsourcing.profile.api.ProfileEntityState;
import com.innovenso.eventsourcing.profile.api.ProfileEvent;
import com.innovenso.eventsourcing.profile.domain.ProfileEntityImpl;
import java.util.function.Supplier;

public interface ProfileEntityRepository
    extends EntityDomainRepository<
        ProfileEntityImpl,
        ProfileCommand,
        ProfileEvent,
        ProfileEntityReadModel,
        ProfileEntityState> {
  @Override
  default Supplier<ProfileEntityImpl> getNewInstanceSupplier() {
    return ProfileEntityImpl::new;
  }

  @Override
  default Supplier<ProfileEntityImpl> getNewInstanceSupplier(EntityId entityId) {
    return () -> new ProfileEntityImpl(entityId);
  }
}
