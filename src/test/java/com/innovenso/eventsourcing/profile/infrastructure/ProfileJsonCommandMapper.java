package com.innovenso.eventsourcing.profile.infrastructure;

import com.innovenso.eventsourcing.api.command.EntityCommand;
import com.innovenso.eventsourcing.infrastructure.JsonCommandMapper;
import com.innovenso.eventsourcing.profile.domain.address.UpdateProfileAddressCommand;
import com.innovenso.eventsourcing.profile.domain.name.UpdateProfileNameCommand;
import lombok.NonNull;

public class ProfileJsonCommandMapper<T extends EntityCommand> extends JsonCommandMapper<T> {
  public ProfileJsonCommandMapper(@NonNull Class<T> commandClass) {
    super(commandClass);
    registerSubType(UpdateProfileNameCommand.class, "updateProfileNameCommand");
    registerSubType(UpdateProfileAddressCommand.class, "updateProfileAddressCommand");
  }
}
