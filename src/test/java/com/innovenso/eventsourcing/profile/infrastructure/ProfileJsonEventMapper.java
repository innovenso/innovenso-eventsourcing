package com.innovenso.eventsourcing.profile.infrastructure;

import com.innovenso.eventsourcing.api.event.EntityEvent;
import com.innovenso.eventsourcing.infrastructure.JsonEventMapper;
import com.innovenso.eventsourcing.profile.domain.address.ProfileAddressUpdatedEvent;
import com.innovenso.eventsourcing.profile.domain.name.ProfileNameUpdatedEvent;
import lombok.NonNull;

public class ProfileJsonEventMapper<T extends EntityEvent> extends JsonEventMapper<T> {
  public ProfileJsonEventMapper(@NonNull Class<T> eventClass) {
    super(eventClass);
    registerSubType(ProfileNameUpdatedEvent.class, "profileNameUpdated");
    registerSubType(ProfileAddressUpdatedEvent.class, "profileAddressUpdated");
  }
}
