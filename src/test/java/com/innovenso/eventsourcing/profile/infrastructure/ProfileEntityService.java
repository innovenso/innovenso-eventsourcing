package com.innovenso.eventsourcing.profile.infrastructure;

import com.innovenso.eventsourcing.api.id.EntityId;
import com.innovenso.eventsourcing.api.id.EntityRevisionId;
import com.innovenso.eventsourcing.api.id.RevisionId;
import com.innovenso.eventsourcing.profile.api.ProfileCommand;
import com.innovenso.eventsourcing.profile.api.ProfileEntityReadModel;
import lombok.NonNull;

public class ProfileEntityService {
  private final ProfileEntityRepository repository;

  public ProfileEntityService(@NonNull final ProfileEntityRepository repository) {
    this.repository = repository;
  }

  public ProfileEntityReadModel load(
      @NonNull final String entityId, @NonNull final String encodedRevisionId) {
    return repository.getReadModel(
        new EntityRevisionId(new EntityId(entityId), RevisionId.decode(encodedRevisionId)));
  }

  public String getLatestRevisionId(@NonNull final String entityId) {
    return repository.getLatestRevisionId(new EntityId(entityId)).getRevisionId().encode();
  }

  public String create() {
    return repository.createEntity().getId().getEntityId().value;
  }

  public String execute(@NonNull final String entityId, @NonNull final ProfileCommand command) {
    return repository
        .getLatestEntityRevision(new EntityId(entityId))
        .execute(command)
        .getRevisionId()
        .encode();
  }
}
