package com.innovenso.eventsourcing.profile.api;

import com.innovenso.eventsourcing.api.event.EntityEvent;

public interface ProfileEvent extends EntityEvent {}
