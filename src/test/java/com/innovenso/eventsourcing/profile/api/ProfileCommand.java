package com.innovenso.eventsourcing.profile.api;

import com.innovenso.eventsourcing.api.command.EntityCommand;

public interface ProfileCommand extends EntityCommand {}
