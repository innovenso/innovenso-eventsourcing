package com.innovenso.eventsourcing.profile.api;

import com.innovenso.eventsourcing.api.entity.EntityReadModel;

public interface ProfileEntityReadModel extends EntityReadModel {
  String getFirstName();

  String getLastName();

  String getFullName();

  Address getAddress();
}
