package com.innovenso.eventsourcing.profile.api;

import com.innovenso.eventsourcing.api.entity.EntityMutableState;

public interface ProfileEntityState extends EntityMutableState, ProfileEntityReadModel {
  void setFirstName(String firstName);

  void setLastName(String lastName);

  void setAddress(Address address);
}
