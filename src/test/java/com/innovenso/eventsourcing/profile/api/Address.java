package com.innovenso.eventsourcing.profile.api;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Address {
  String street;
  String number;
  String postalCode;
  String city;
}
